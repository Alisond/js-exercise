alert('¡Bienvenido al juego Adivina el número!');
let maxNumber = 100;
let secretNumber = Math.floor(Math.random()*maxNumber)+1;
let limitAtempt = parseInt(prompt('Ingresa el máximo de intentos que quieres tener disponibles: '));
let atempts = limitAtempt;
let userNumber = prompt(`Ingresa un número del 1 al ${maxNumber}: `);
let count = 0;

console.log(typeof(atempts));

while (numberValidation(userNumber, secretNumber) != true) {
    count++;
    atempts--;
    if(count >= limitAtempt){
        alert('Lástima, no te quedan más intentos. Reinicia para jugar de nuevo.');
        break;
    }else {
        alert(`Inténtalo de nuevo con un número ${userNumber < secretNumber ? 'mayor' : 'menor'}, intentos disponibles ${atempts}`);
        userNumber = prompt('Ingresa un número: ');
    }
}

function numberValidation(userNumber, secretNumber){
    if(userNumber == secretNumber){
        count++;
        alert(`Felicidades, Adivinaste en ${count} ${count == 1 ? 'Intento' : 'Intentos'}. El número secreto era: ${secretNumber}`)
        return true; 
    } else
    return false;
}



